
function encrypt($plaintext, $user)
{
    $password = $users[$user];
    $method = "AES-256-CBC";
    $key = hash('sha256', $password, true);
    $iv = openssl_random_pseudo_bytes(16);
    $time = strval(microtime(true) * 10000);
    $user = urlencode($user);
    $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
    $hash = hash_hmac('sha256', $ciphertext, $key, true);

    return base64_encode($iv) . '|' . $time . '|' . base64_encode($hash) . '|' . $user . '|' . base64_encode($ciphertext);
}

function decrypt($ivHashCiphertext)
{
    $method = "AES-256-CBC";
    $a = explode('|', $ivHashCiphertext);
    $iv = $a[0];
    $time = $a[1];
    $hash = $a[2];
    $user = $a[3];
    $ciphertext = $a[4];

    echo ($iv . '<br>');
    echo ($time . '<br>');
    echo ($hash . '<br>');
    echo ($time . '<br>');
    echo ($user . '<br>');
    echo ($ciphertext . '<br>');

    $user = urldecode($user);
    $time = intval($time);
    $password = $users[$user];
    $key = hash('sha256', $password, true);
    $iv = base64_decode($iv);
    $hash = base64_decode($hash);
    $ciphertext = base64_decode($ciphertext);

    if (hash_hmac('sha256', $ciphertext, $key, true) !== $hash) {
        return null;
    }

    return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
}