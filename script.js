class FD {
    constructor() {
        this.container = null;
        this.controls = null;
        this.camera = null;
        this.scene = null;
        this.renderer = null;
    }

    init() {
        var NEAR = 1e-6, FAR = 1e27;
        this.camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, NEAR, FAR);
        this.camera.position.z = 200;

        this.controls = new THREE.TrackballControls(this.camera);

        this.scene = new THREE.Scene();
        this.scene.add(new THREE.HemisphereLight());

        var directionalLight = new THREE.DirectionalLight(0xffeedd);
        directionalLight.position.set(0, 0, 2);
        this.scene.add(directionalLight);
        var materialColor = new THREE.Color();
        materialColor.setRGB(0.5, 1.0, 1.0);

        var wireMaterial = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, wireframe: false });

        //3ds files dont store normal maps
        //var loader = new THREE.TextureLoader();
        //var normal = loader.load( 'n/normal.jpg' );

        //var loaderTDS = new THREE.TDSLoader();
        var loaderOBJ = new THREE.OBJLoader();
        var loaderMTL = new THREE.MTLLoader();
        // .setPath( 'models/obj/male02/' )
        // .load( 'male02_dds.mtl', function ( materials ) {}
        //loader.setResourcePath('models/3ds/portalgun/textures/');
        //var loadTDS = name => loaderTDS.load(name, object => this.scene.add(object));


        var loadOBJ = name => loaderOBJ.load(name, object => {

            // object.setMaterials()
            object.position.x = Math.random() * 1000;
            object.position.y = Math.random() * 1000;
            object.position.z = Math.random() * 1000;
            //object.material = wireMaterial;
            object.traverse(function (child) {

                //  if (child.isMesh) child.material.wireframe = true;

            });
            //mtl_name = name.match(/^(.*\.)[^.]*$/)[1] + 'mtl';
            /*
                            loaderMTL.load(mtl_name, function (materials) {
                                materials.preload();
                                object.setMaterials( materials );
                            });
                            */
            this.scene.add(object);
        });
        loaderMTL.setPath('/obj/');
        loaderOBJ.setPath('/obj/');

        var loadMTL = name => new Promise(ok => loaderMTL.load(name, ok))

        fetch('/obj').then(res => res.json()).then(obj => {
            Promise.all(
                obj.dirs
                    .filter(a => a.endsWith('.mtl'))                    
                    .map(n => loadMTL(n).then(m => {
                        m.preload();
                        return m;
                    })))
                .then(materials => {
                    // materials.preload();
                    loaderOBJ.setMaterials(materials);
                })
                .then(_ => {
                    obj.dirs.filter(a => a.endsWith('.obj'))
                        .forEach(name => {
                            loadOBJ(name);
                        });
                });
        });

        this.container = document.createElement('div');
        document.body.appendChild(this.container);
        this.renderer = new THREE.WebGLRenderer({ logarithmicDepthBuffer: true });
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.container.appendChild(this.renderer.domElement);
        window.addEventListener('resize', () => this.resize(), false);
    }

    resize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.setPixelRatio(window.devicePixelRatio);
    }

    animate() {

        this.controls.update();
        this.renderer.render(this.scene, this.camera);

        requestAnimationFrame(() => this.animate());

    }
}

var fd = new FD();

fd.init();
fd.animate();
